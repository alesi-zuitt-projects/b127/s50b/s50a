// Bootstrap
import { Jumbotron, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'
//react
import {useContext} from 'react'
//MUI
import { purple } from '@mui/material/colors';
import Button from '@mui/material/Button';
import * as React from 'react';
import { experimentalStyled as styled } from '@mui/material/styles'

//usercontext
import UserContext from '../UserContext';


const ColorButton = styled(Button)(({ theme }) => ({
	color: theme.palette.getContrastText(purple[500]),
	backgroundColor: purple[500],
	'&:hover': {
	  backgroundColor: purple[700],
	},
  }));


export default function Banner(){
	const {user} = useContext(UserContext)
	return(
		<Row>
			<Col className="mt-3">
				<Jumbotron>
					<h1  className= "text-dark">Shoepeeps x Groovy Retrostyle</h1>
					<p  className= "text-dark">this is a sample project for capstone</p>
					
					{
						user.accessToken !== null ?
						<Link to={`/products`}>
						<ColorButton variant="contained" type="submit" id="submitBtn" >Shop Now</ColorButton>
						</Link>
						:
						<Link to={`/login`}>
						<ColorButton variant="contained" type="submit" id="submitBtn" >Login</ColorButton>
						</Link>

					}
					
				</Jumbotron>
			</Col>
		</Row>
		)
}