// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
import {Fragment, useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom'
//ReactContext
import UserContext from '../UserContext'
import  './Navbar.css'

export default function AppNavbar(){
	const { user } = useContext(UserContext)

	let rightNav = (user.accessToken !== null) ?
		<Fragment>
		<Nav.Link as = {NavLink} to= '/cart' className= "text-dark" > My Cart </Nav.Link>

		<Nav.Link as = {NavLink} to= '/order' className= "text-dark" > My Orders </Nav.Link>

		<Nav.Link as = {NavLink} to= '/logout' className= "text-dark" > Logout </Nav.Link>

		

		</Fragment>
		:
		<Fragment>
			<Nav.Link as={NavLink} to='/register' className= "text-dark" >Register</Nav.Link>

			<Nav.Link as={NavLink} to='/login' className= "text-dark">Login</Nav.Link>
		</Fragment>


	return(
		<Navbar expand="lg">
			<Navbar.Brand as={Link} to='/' className= "text-dark">Shoepeeps x Groovy Retrostyle</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto " >
					<Nav.Link as={NavLink} to='/' className= "text-dark">Home</Nav.Link>

					<Nav.Link as={NavLink} to='/products' className= "text-dark">Products</Nav.Link>
				</Nav>

				<Nav className="ml-auto" >
					{rightNav}
				</Nav>

			</Navbar.Collapse>
		</Navbar>

		)
}
