

import {Fragment} from 'react';
import {Table, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'






export default function UserOrderView({orderData}){
    const {_id, productId, productName, sizeOrder , quantity, totalPerProduct, purchasedOn} = orderData;
    console.log(orderData)

    return(
        <Fragment>
		<Table striped hover>
                <tbody className='hover'>
                    <tr key={_id} className='text-center'>
                        <td width='16.66%'>{purchasedOn.slice(0,10)}</td>
                        <td width='16.66%'>{productId}</td>
						<td width='16.66%'>{productName}</td>
						<td width='16.66%'>{sizeOrder}</td>
                        <td width='16.66%'>{quantity}</td>
                        <td width='16.66%'>{totalPerProduct}</td>
                    </tr>
                </tbody>
		</Table>
        </Fragment>   
        
    )
}