import {useState, useEffect} from 'react'
import { Container, Row} from 'react-bootstrap'
import ProductCard from './ProductCard'

export default function UserView({ productsData }){

	const [products, setProducts] = useState([])
	
	useEffect(() => {
		const productsArray = productsData.map(product  => {
			//only render active courses
			if(product.isActive === true) {
				return(
					< ProductCard productProp={product} key = {product._id} breakPoint={4} />

					)
			}else{
				return null;
			}
		})
		//set the courses state to the result of our map function, to bring our returned course components outside of the scope of oure useEffect where our return statement below can see it
		setProducts(productsArray)
	}, [productsData])




	return(
		<Container>
			
			<h1 className="text-center">Products</h1>
			<Row>
				{products}
			</Row>			
			 
		</Container>

		)
}