// Bootstrap
import {Carousel, Image } from 'react-bootstrap';



export default function Highlights() {
	return(
		
		<Carousel>
			<Carousel.Item>
				<img
				className="d-block w-100"
				src="../images/image2.jpg"
				alt="First slide"
				/>
			</Carousel.Item>
			<Carousel.Item>
				<Image 
				className="d-block w-100"
				src="../images/image1.jpeg" fluid
				alt="Second slide"
				/>
			</Carousel.Item>
			<Carousel.Item>
				<img
				className="d-block w-100"
				src="../images/image3.jpeg"
				alt="Third slide"
				/>
				
			</Carousel.Item>
			
		</Carousel>
	)
}