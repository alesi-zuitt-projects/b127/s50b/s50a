import {useState, useEffect, Fragment} from 'react';
import {Table, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2';

//MUI
import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Typography from '@mui/material/Typography';



export default function UserCart (props){
    const {productData, productPrice, fetchProduct} = props
    const [hasData, setHasData] = useState('');
    const [cartItems, setCartItems] = useState('')

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
      };


    const removeOrder = (productId) => {
        fetch('http://localhost:4000/cart/removeOrder',{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                productId: productId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                fetchProduct()
                Swal.fire({
                    title: 'Successfully removed',
                    icon: 'success'
                })
            }else{
                Swal.fire({
                    title: 'Something went wrong.',
                    icon: 'error',
                    text: 'Please contact customer support.'
                })
            }
        })
    }

    const Order = (e) => {
        fetch('http://localhost:4000/orders/createOrder',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data){
                fetchProduct();
                Swal.fire({
                    title: 'Successfully Ordered.',
                    icon: 'success'
                })
            }else{
                Swal.fire({
                    title: 'Something went wrong.',
                    icon: 'error'
                })
            }
        })
    }

    

    useEffect(() => {
        if(productData.length === 0){
            setHasData(false)
        }else{
            const cartArray = productData.map(product => {
                return (
                    <tr key={product.productId}>
                        <td>{product.dateAdded.slice(0,10)}</td>
                        <td>{product.productName}</td>
                        <td>{product.sizeOrder}</td>
                        <td>{product.quantity}</td>
                        <td>{product.totalPerProduct}</td>
                        <td>
                            <Button variant= 'danger' onClick={() => removeOrder(product.productId)}>Remove</Button>
                        </td>
                    </tr>
                )
            })
            setHasData(true)
            setCartItems(cartArray)

        }
    },[productData])
    return  (

        (hasData) ?
        <Fragment>
            <div>
                <h1>Cart Items</h1>
            </div>
            <Table>
                <thead>
                    <tr>
                        <td> Date Added </td>
                        <td> Name </td>
                        <td> Size </td>
                        <td> Quantity </td>
                        <td> Total</td>
                    </tr>
                    
                </thead>
                <tbody>
                    {cartItems}
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>{productPrice}</td>
                        <td>
                            <Button variant = 'primary' onClick={(e) => Order(e)}>Pay Now</Button>
                        </td>
                    </tr>
                </tfoot>
            </Table>

            <div>
                <Button onClick={handleOpen}>Open modal</Button>
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    open={open}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                    timeout: 500,
                    }}
                >
                    <Fade in={open}>
                    <Box sx={style}>
                        <Typography id="transition-modal-title" variant="h6" component="h2">
                        Text in a modal
                        </Typography>
                        <Typography id="transition-modal-description" sx={{ mt: 2 }}>
                        Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                        </Typography>
                    </Box>
                    </Fade>
                </Modal>
            </div>
        </Fragment>
        :
        <Fragment>
            <div>
                <h1>Empty Cart, to view our products click <Link to="/products">Here!!!</Link></h1>
            </div>
        </Fragment>
        
        
    )

}