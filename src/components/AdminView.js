import {useState, useEffect, Fragment} from 'react'
import {Table, Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'
//MUI

import * as React from 'react';
import Button from '@mui/material/Button';

import { purple } from '@mui/material/colors';

import { experimentalStyled as styled } from '@mui/material/styles'




const ColorButton = styled(Button)(({ theme }) => ({
	color: theme.palette.getContrastText(purple[500]),
	backgroundColor: purple[500],
	'&:hover': {
	  backgroundColor: purple[700],
	},
  }));


export default function AdminView(props){	
	const {productsData, fetchData} = props
	const [products,setProducts] = useState([])
	//add state for addCourse
	const [brandName, setBrandName] = useState('')
	const [productName, setProductName] = useState('')
	const [color, setColor] = useState('')
	const [size, setSize] = useState('')
	const [price, setPrice] = useState('')
	//state to modal add
	const [showAdd, setShowAdd] = useState(false)	
	//open a subject true or false kung mag show
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	
	//state for edit course
	const [showEdit, setShowEdit] = useState(false)
	//add state for courseId
	const[productId, setProductId] = useState('')
	// close and openedit for modals Update
	const closeEdit = () => {
		setShowEdit(false)
		setBrandName('')
		setProductName('')
		setSize()
		setColor('')
		setPrice(0)
	}
	const openEdit = (productId) => {
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			//populate all input values with the course information
			setProductId(data._id)
			setBrandName(data.brandName)
			setProductName(data.productName)
			setColor(data.color)
			setSize(data.size)
			setPrice(data.price)
		})
		//then open modal
		setShowEdit(true)
	} 
	


	
	useEffect(() => {
	const productsArray = productsData.map(product => {
	return(
		<tr key={product.id}>
			<td>{product._id}</td>
			<td>{product.brandName}</td>
			<td>{product.productName}</td>
			<td>{product.color}</td>
			<td>{product.size}</td>
			<td>{product.price}</td>
			<td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Available" : "Unavailable"}</td>
			<td>
				<ColorButton variant = "primary" size= "sm" onClick={ () => openEdit(product._id)}> Update </ColorButton>
			</td>
			<td>
			{product.isActive ?
				<ColorButton variant = "danger" size = "sm" onClick={	()	=> archiveToggle(product._id, product.isActive)	}> Disable </ColorButton> : <ColorButton variant = "success" size = "sm" onClick={	()	=> activateToggle(product._id, product.isActive)	}> Enable </ColorButton>
			}				
			</td>
		</tr>
		)
	})
	setProducts(productsArray)
}, [productsData])

	const addProduct = (e) => {
		e.preventDefault();
		fetch('http://localhost:4000/products/createProduct',{
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				brandName: brandName,
				productName: productName,
				color: color,
				size: size,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				//run our fecthdata function that we passed from our parent component in order to re render our page
				fetchData()
				Swal.fire({
					title: 'Successfully Added!',
					icon: 'success',
					text: 'Product will be viewable by students'
				})
				setBrandName('')
				setProductName('')
				setSize(0)
				setColor('')
				setPrice(0)
				// autoclose modals
				closeAdd()
				
			}else{
				Swal.fire({
					title: "Can't add product",
					icon: "error",
					text: "make sure valid inputs"
				})

			}
		})
	}
	// Edit course function
	const editProduct = (e, productId) => {
		e.preventDefault()
		fetch(`http://localhost:4000/products/${productId}/update`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("accessToken")}`
			},
			body: JSON.stringify({
				brandName: brandName,
				productName: productName,
				color: color,
				size: size,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Successfully Updated!',
					icon: 'success',
					text: 'Product is updated!!!'
				})
				closeEdit()
			}else{
				fetchData()
				Swal.fire({
					title: "Can't update product",
					icon: "error",
					text: "make sure valid inputs"
				})
			}
		})
	}
	
	//Archive a course
	const archiveToggle = (productId, isActive) => {
		fetch(`http://localhost:4000/products/${productId}/archive`,{
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem("accessToken")}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title:"Successfully archived!!!",
					icon: "success",
					text:"product successfully disabled"
				})
				
			}else{
				fetchData()
				Swal.fire({
					title:"Error!!!",
					icon: "error",
					text: "archive error!!!"
				})
			}
		})
	}
	//Activate a course
	const activateToggle = (productId, isActive) => {
		fetch(`http://localhost:4000/products/${productId}/activate`,{
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem("accessToken")}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title:"Successfully activated!!!",
					icon: "success",
					text:"product successfully activated"
				})
				
			}else{
				fetchData()
				Swal.fire({
					title:"Error!!!",
					icon: "error",
					text: "product error!!!"
				})
			}
		})
	}
	return(
		<Fragment>
		 


		<div className="text-center my-4"> 
			<h2>Admin Dashboard</h2>
			<div className= "d-flex justify-content-center">
			<ColorButton variant="primary" onClick = {openAdd}> Add Product </ColorButton>
			</div>
		</div>
		<Table striped bordered hover responsive className="container-fluid">
			<thead className="text-dark text-center">
				<tr>
					<th width= "11.11%">ID</th>
					<th width= "11.11%">Brand Name</th>
					<th width= "11.11%">Product Name</th>
					<th width= "11.11%">Color Way</th>
					<th width= "11.11%"> Size </th>
					<th width= "11.11%">Price</th>
					<th width= "11.11%">Availability</th>
					<th width= "11.11%">Actions</th>
					<th width= "11.11%">Enable/Disable</th>
				</tr>


			</thead>
			<tbody>
			{products}

			</tbody>

		</Table>

		<Modal show = {showAdd} onHide= {closeAdd}>
			<Form onSubmit = {e => addProduct(e)}>
				<Modal.Header>
					<Modal.Title>
						Add Product
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
						<Form.Group>
							<Form. Label>Brand Name:</Form. Label>
							<Form.Control type = "text" value = {brandName} onChange= {e => setBrandName(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Product Name:</Form. Label>
							<Form.Control type = "text" value = {productName} onChange= {e => setProductName(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Color Way:</Form. Label>
							<Form.Control type = "text" value = {color} onChange= {e => setColor(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Size:</Form. Label>
							<Form.Control type = "text" value = {size} onChange= {e => setSize(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Price:</Form. Label>
							<Form.Control type = "number" value = {price} onChange= {e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
					<ColorButton variant="secondary" onClick = {closeAdd}> close </ColorButton>
					<ColorButton variant="success" type = "submit"> Add Product </ColorButton>
					</Modal.Footer>
			</Form>
		</Modal>



		<Modal show = {showEdit} onHide= {closeEdit}>
			<Form onSubmit = {e => editProduct(e, productId)}>
				<Modal.Header closeButton>
					<Modal.Title>
						Update Product
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group>
							<Form. Label>Brand Name:</Form. Label>
							<Form.Control type = "text" value = {brandName} onChange= {e => setBrandName(e.target.value)}  required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Product Name:</Form. Label>
							<Form.Control type = "text" value = {productName} onChange= {e => setProductName(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Color Way:</Form. Label>
							<Form.Control type = "text" value = {color} onChange= {e => setColor(e.target.value)} required  />
						</Form.Group>
						<Form.Group>
							<Form. Label>Size:</Form. Label>
							<Form.Control type = "text" value = {size} onChange= {e => setSize(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Price:</Form. Label>
							<Form.Control type = "number" value = {price} onChange= {e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<ColorButton variant="secondary" onClick = {closeEdit}> Close </ColorButton>
						<ColorButton variant="success" type="submit"> Submit </ColorButton>
					</Modal.Footer>
			</Form>
		</Modal>
		</Fragment>
		)
}