// Bootstrap
import { Card, Col } from 'react-bootstrap';

import * as React from 'react';
import { experimentalStyled as styled } from '@mui/material/styles'

import { purple } from '@mui/material/colors';
import Button from '@mui/material/Button';





import PropTypes from 'prop-types'

import { Link } from 'react-router-dom'

//MUI



export default function ProductCard({productProp, breakPoint}) {
	const {_id, brandName, productName, price} = productProp

	  const ColorButton = styled(Button)(({ theme }) => ({
		color: theme.palette.getContrastText(purple[500]),
		backgroundColor: purple[500],
		'&:hover': {
		  backgroundColor: purple[700],
		},
	  }));




	return(
		<React.Fragment>
		
				
						
							<Col xs={12} md={breakPoint} className= "mt-3 mb-3">
								
										<Card className="cardHighlight m-3"  >
										<Card.Header><h2>{brandName}</h2></Card.Header>
											<Card.Body>
												<Card.Title><h3>{productName}</h3></Card.Title>
												<Card.Subtitle><h4>{price}php</h4></Card.Subtitle>
												
												<Card.Footer>
												
												<Link to={`/products/${_id}`} >
												<ColorButton variant="contained"  >View Item</ColorButton>
												</Link>
												</Card.Footer>	
											</Card.Body>
										</Card>	
								 
							</Col>
						
			
		</React.Fragment>
		  );
	
}

		



ProductCard.propTypes = {

	productProp: PropTypes.shape({
		
		brandName: PropTypes.string.isRequired,
		productName: PropTypes.string.isRequired,
		color: PropTypes.string.isRequired,
		size: PropTypes.number.isRequired,
		price: PropTypes.number.isRequired
	})
}

