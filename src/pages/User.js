import UserContext from "../UserContext";
//Admin View
import AdminView from "../components/AdminView";
//reactBootstrap
import {Container} from "react-bootstrap";
//useEffect
import {useEffect, useState} from "react";


export default function User () {
    
    const [allUsers, setAllUsers] = useState([])
    const fetchData = () => {
        fetch('http://localhost:4000/getUsers',{
            header: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("accessToken")}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setAllUsers(data)
            })
        }

        useEffect(() =>{
            fetchData()
        }, [])

        return(
            <Container fluid>
                {

                   <AdminView usersData= {allUsers} fetchData={fetchData}/>
                    
                }

            </Container>
        )
    }
