
import {useState, useEffect, useContext, Fragment} from 'react'

//MUI
import { purple } from '@mui/material/colors';
import Button from '@mui/material/Button';
import * as React from 'react';
import { experimentalStyled as styled } from '@mui/material/styles'

//bootstrap
import {Table} from 'react-bootstrap'

//react context
import UserContext from '../UserContext'

import UserOrderView from '../components/UserOrderView'

import {Link} from 'react-router-dom'

const ColorButton = styled(Button)(({ theme }) => ({
	color: theme.palette.getContrastText(purple[500]),
	backgroundColor: purple[500],
	'&:hover': {
	  backgroundColor: purple[700],
	},
  }));

export default function Products() {

	const {user} = useContext(UserContext);

	
    const [orders, setOrders] = useState([])
    


    const fetchOrder = () =>{
		fetch('http://localhost:4000/orders/myOrders', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res=>res.json())
		.then(data=> {
			if (data.length!==0) {
				const orderArr = data.map(order=>{
					return <UserOrderView orderData={order}/>
				})
				setOrders(orderArr);
			} else {
				setOrders();
			}
		})
	}

	useEffect(()=> {
		fetchOrder()
	},[])

	return(
		
        orders==null?
        <div className='text-center pt-5'>
            <h4>No Existing Orders. Redirect <Link to='/products'>here</Link> to view our products.</h4>
        </div>
        :
        <Fragment>
			<Table>
				<thead className='bg-dark text-light'>
					<tr className='text-center'>
                        <td width='16.66%'>Date Ordered</td>
                        <td width='16.66%'>ProductId</td>
						<td width='16.66%'>Name</td>
						<td width='16.66%'>Size</td>
                        <td width='16.66%'>Quantity</td>
                        <td width='16.66%'>Total</td>
					</tr>
				</thead>
			</Table>

            {orders}
			
        </Fragment>
	)
}