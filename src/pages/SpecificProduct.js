import { useState, useEffect, useContext} from 'react'
import { Container, Card, Form, InputGroup} from 'react-bootstrap'
import Swal from 'sweetalert2'

import { Link, useParams} from 'react-router-dom'

import UserContext from '../UserContext';
//mui

import Button from '@mui/material/Button';
import { purple } from '@mui/material/colors';
import * as React from 'react';
import { experimentalStyled as styled } from '@mui/material/styles'
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';


export default function SpecificProduct(){
    const {user} = useContext(UserContext)
    const [brandName, setBrandName] = useState('')
	const [productName, setProductName] = useState('')
	const [color, setColor] = useState('')
	const [size, setSize] = useState('')
	const [price, setPrice] = useState(0)
    const {productId} = useParams()
    const [quantity, setQuantity] = useState(1)

    //CSS MUI
		const ColorButton = styled(Button)(({ theme }) => ({
			color: theme.palette.getContrastText(purple[500]),
			backgroundColor: purple[500],
			'&:hover': {
			  backgroundColor: purple[700],
			},
		  }));
    //Mui Select Size
        const [sizeOrder, setSizeOrder] = React.useState('');

        const handleChange = (event) => {
        setSizeOrder(event.target.value);
        };


    const increaseQuantity = e => {
		if (quantity    <   99) {
			setQuantity(quantity    +   1)
		}
    }
    const decreaseQuantity = e => {
        if (quantity >= 1){
            setQuantity(quantity - 1)
        }
    }

	


    useEffect(() => {
        fetch(`http://localhost:4000/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            setBrandName(data.brandName)
            setProductName(data.productName)
            setColor(data.color)
            setSize(data.size)
            setPrice(data.price)
        })
    }, [])


    const order = () => {
        
        fetch('http://localhost:4000/cart/addToCart',{
            method: 'POST',
            headers:{
                'Content-Type': 'application/json',
                Authorization:`Bearer ${localStorage.getItem('accessToken') }`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity,
                sizeOrder: sizeOrder
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true ){
                Swal.fire({
                    title: "Ordered",
                    icon: "success",
                    text: `successfully ordered ${productName}`
                })
            }else {
                Swal.fire({
                    title: "Failed",
                    icon: "error",
                    text: `failed to order ${productName}`
                })
            }
        })
    }

    return(
        <Container>
            <Card className= "container-xs">
                <Card.Header className="bg-dark text-white text-center pb-0">
                    <h3>{brandName}</h3>
                </Card.Header>
                <Card.Body className = "text-center" >
                    <h4>
                        {productName}
                    </h4>
                    <h4>{size}</h4>
                    <h4>Php {price}</h4>
                    <h4>{color}</h4>
                    <div>

                    

                    <Box sx={{ minWidth: 120 }}>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Size</InputLabel>
                            <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={sizeOrder}
                            label="Size"
                            onChange={handleChange}
                            >
                            <MenuItem value="XS" >XS</MenuItem>
                            <MenuItem value="S">S</MenuItem>
                            <MenuItem value="M" >M</MenuItem>
                            <MenuItem value="L">L</MenuItem>
                            <MenuItem value="XXL">XL</MenuItem>
                            <MenuItem value="XXXL">XXL</MenuItem>
                            
                            </Select>
                        </FormControl>
                    </Box>
					    	
					<InputGroup className= "w-25 pt-3">
                    <InputGroup.Prepend>
                           <ColorButton  onClick={e=>decreaseQuantity(e)}>-</ColorButton>
                    </InputGroup.Prepend>
					   	<Form.Control 
					   		className='text-center w-25' 
					   		value={quantity} 
					   		type='number'
					   		maxLength='2' 
					   		onChange={e=>{
                                   if(e.target.value!=='') {
					   				setQuantity(e.target.value)
					   			}
					   			else{
					   				setQuantity(0)
					   			}
					   		}}
					   	/>
                           
						<InputGroup.Append>
                            <ColorButton onClick={e=>increaseQuantity(e)}>+</ColorButton>
                        </InputGroup.Append>
                            
                        </InputGroup>
					</div>

               

                </Card.Body>
                <Card.Footer className= "bg-dark text-center">      
                    {user.accessToken !== null ?
                    <ColorButton variant="success"  onClick={() => order(productId, quantity, sizeOrder)}>Add to cart</ColorButton>
                    :
                    <ColorButton>
                    <Link className= "btn btn-block text-light" to="/login"> Login to shop </Link>
                    </ColorButton>
                    }
                    
                </Card.Footer>
            </Card>
        </Container>
    )
}   